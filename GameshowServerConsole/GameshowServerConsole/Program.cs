﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SignalR.Client.Hubs;
using System.Threading.Tasks;
using System.Threading;

namespace GameshowServerConsole
{
    class Program
    {
        static HubConnection williamBConnection;
        static HubConnection multiplayerQuizConnection;
        static IHubProxy v1proxy;
        static IHubProxy v2proxy;
        static IHubProxy v3proxy;

        static void Main(string[] args)
        {
            System.Net.ServicePointManager.Expect100Continue = false;

            williamBConnection = new HubConnection("http://www.williamb.net");
            multiplayerQuizConnection = new HubConnection("http://MultiplayerQuiz.Apphb.com");

            multiplayerQuizConnection.Error += (e) =>
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(e.ToString());
                };
            williamBConnection.Error += (e) =>
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine(e.ToString());
                };

            v1proxy = williamBConnection.CreateProxy("Gameshow.Server.SignalR.GameShowHub");
            v1proxy.On<string>("MessageReceived", message =>
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(message);
                });

            ManualResetEvent waitForOk = new ManualResetEvent(false);
            v2proxy = williamBConnection.CreateProxy("Gameshow.Server.SignalR.GameShow");
            v2proxy.On<string>("MessageReceived", message =>
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(message);
            });
            v2proxy.On("CarryOnThen", () =>
            {
                if (waitForOk != null)
                {
                    waitForOk.Set();
                }
            });


            williamBConnection.Start().Wait();

            var v1adminTask = v1proxy.Invoke<string>("AddAdminConsole");
            v1adminTask.Wait();

            Console.WriteLine("v1 hopefully connected");


            bool connectionSuccessful = false;
            do
            {
                var v2adminTask = v2proxy.Invoke<string>("AddAdminConsole");
                v2adminTask.Wait();

                connectionSuccessful = waitForOk.WaitOne(5000);
            } while (!connectionSuccessful);
            Console.WriteLine("V2 connected");


            waitForOk = new ManualResetEvent(false);
            v3proxy = multiplayerQuizConnection.CreateProxy("Gameshow.Server.SignalR.GameShow");
            v3proxy.On<string>("MessageReceived", message =>
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(message);
            });
            v3proxy.On("CarryOnThen", () =>
            {
                if (waitForOk != null)
                {
                    waitForOk.Set();
                }
            });

            connectionSuccessful = false;
            do
            {
                multiplayerQuizConnection.Start().Wait();
                var v3adminTask = v3proxy.Invoke<string>("AddAdminConsole");
                v3adminTask.Wait();

                connectionSuccessful = waitForOk.WaitOne(5000);
            } while (!connectionSuccessful);

            Console.WriteLine("v3 Connected");

            bool quit = false;
            do
            {
                var input = Console.ReadLine();

                switch(input.ToUpperInvariant())
                {
                    case "CLS":
                        Console.Clear();
                        break;

                    case "QUIT":
                    case "":
                        quit = true;
                        break;
                }

            } while (!quit);

        }
    }
}