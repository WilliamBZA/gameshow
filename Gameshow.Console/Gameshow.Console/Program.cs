﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameShow;

namespace Gameshow.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var game = new Game(null, 5, 5, 0))
            {
                game.GameOver += new Game.GameOverHandler(game_GameOver);
                game.NewQuestion += new Game.NewQuestionHandler(game_NewQuestion);
                game.QuestionFinished += new Game.QuestionFinishedHandler(game_QuestionFinished);

                game.StartGame();
                System.Console.ReadLine();
            }

            GC.Collect();

        }

        static void game_QuestionFinished(object sender, QuestionFinishedEventArgs e)
        {
            throw new NotImplementedException();
        }

        static void game_NewQuestion(object sender, QuestionEventArgs e)
        {
            System.Console.WriteLine("New Question: {0}", e.Question.QuestionText);
        }

        static void game_GameOver(object sender, GameOverEventArgs e)
        {
            System.Console.WriteLine("GAME OVER");
        }
    }
}
