﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow.Messages
{
    public class NewQuestion
    {
        public int QuestionNumber { get; set; }

        public int TotalQuestions { get; set; }

        public string QuestionTitle { get; set; }

        public Dictionary<string, string> PossibleAnswers { get; set; }

        public string QuestionExplanation { get; set; }

        public int Duration { get; set; }
    }
}