﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow.Messages
{
    public partial class PlayerWrapper
    {
        public string ClientId { get; set; }

        public string PlayerName { get; set; }

        public string GravatarEmail { get; set; }

        public decimal Score { get; set; }

        public int Order { get; set; }

        public bool LastQuestionCorrect { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", PlayerName, Score);
        }
    }
}