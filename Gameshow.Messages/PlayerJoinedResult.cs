﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow.Messages
{
    public class PlayerJoinedResult
    {
        public string ClientId { get; set; }

        public int EstimatedSecondsTillNextQuestion { get; set; }

        public string RedirectionUrl { get; set; }

        public string Version { get; set; }
    }
}