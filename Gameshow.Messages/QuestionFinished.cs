﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow.Messages
{
    public class QuestionFinished
    {
        public int QuestionNumber { get; set; }

        public List<PlayerWrapper> IncorrectPlayers { get; set; }

        public List<PlayerWrapper> CorrectPlayers { get; set; }

        public List<PlayerWrapper> UnansweredPlayers { get; set; }

        public int EstimatedTimeTillNextQuestion { get; set; }
    }
}