﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow.Messages
{
    public class AnswerQuestionResult
    {
        public int EstimatedSecondsTillQuestionEnd { get; set; }
    }
}
