﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public class QuestionFinishedEventArgs : EventArgs
    {
        public QuestionFinishedEventArgs(int questionNumber, Dictionary<Player, TimeSpan> correctPlayers, List<Player> incorrectPlayers, List<Player> unansweredPlayers)
        {
            QuestionNumber = questionNumber;
            CorrectPlayers = correctPlayers;
            IncorrectPlayers = incorrectPlayers;
            UnansweredPlayers = unansweredPlayers;
        }

        public int QuestionNumber { get; set; }

        public List<Player> IncorrectPlayers { get; set; }

        public Dictionary<Player, TimeSpan> CorrectPlayers { get; set; }

        public List<Player> UnansweredPlayers { get; set; }
    }
}
