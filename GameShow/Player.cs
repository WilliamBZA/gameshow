﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public class Player : IComparable, IComparable<Player>
    {
        public Player(string playerName, string gravatarEmail)
        {
            PlayerName = playerName;
            GravatarEmail = gravatarEmail;
        }

        public int CompareTo(object obj)
        {
            Player other = obj as Player;
            if (other == null)
            {
                return -1;
            }

            return CompareTo(other);
        }

        public int CompareTo(Player other)
        {
            return ClientId.CompareTo(other.ClientId);
        }

        public string PlayerName { get; set; }

        public string ClientId { get; set; }

        public decimal Score { get; set; }

        public string GravatarEmail { get; set; }
    }
}