﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public class QuestionEventArgs : EventArgs
    {
        public QuestionEventArgs(Question question, int questionNumber)
        {
            Question = question;
            QuestionNumber = questionNumber;
        }

        public Question Question { get; set; }

        public int QuestionNumber { get; set; }
    }
}