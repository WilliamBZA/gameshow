﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public class QuestionOption
    {
        public string Text { get; set; }

        public bool IsCorrect { get; set; }
    }
}