﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public class Question
    {
        public Question()
        {
            Options = new Dictionary<int, QuestionOption>();
            CorrectPlayers = new Dictionary<Player, TimeSpan>();
            IncorrectPlayers = new List<Player>();

            State = QuestionState.None;
            TimeCreated = DateTime.Now;
        }

        public int Id { get; set; }

        public string QuestionText { get; set; }

        public string QuestionExplanation { get; set; }

        public Dictionary<Player, TimeSpan> CorrectPlayers { get; set; }

        public List<Player> IncorrectPlayers { get; set; }
        
        public Dictionary<int, QuestionOption> Options { get; set; }

        public void QuestionAsked()
        {
            TimeAsked = DateTime.Now;
            State = QuestionState.Asked;
        }

        public void AnswerQuestion(Player player, int answerNumber)
        {
            if (State != QuestionState.Asked)
            {
                return;
            }

            if (!PlayerHasAnswered(player))
            {
                var correct = Options[answerNumber].IsCorrect;

                if (correct)
                {
                    DateTime timeAnswered = DateTime.Now;
                    CorrectPlayers.Add(player, timeAnswered - TimeAsked);

                    player.Score += 100;
                    int correctPlayerCount = CorrectPlayers.Count;

                    if (correctPlayerCount <= 3)
                    {
                        player.Score += (4 - correctPlayerCount) * 100;
                    }
                }
                else
                {
                    IncorrectPlayers.Add(player);
                }
            }
        }

        private bool PlayerHasAnswered(Player player)
        {
            return CorrectPlayers.ContainsKey(player) || IncorrectPlayers.Contains(player);
        }

        public QuestionState State { get; private set; }

        public DateTime TimeAsked { get; private set; }

        public DateTime TimeFinished { get; private set; }

        public DateTime TimeCreated { get; private set; }

        public void Finished()
        {
            State = QuestionState.Finished;
            TimeFinished = DateTime.Now;
        }
    }
}