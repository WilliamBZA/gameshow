﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Gameshow
{
    public class Game : IDisposable
    {
        private IQuestionRepository _questionRepo;
        private Dictionary<string, Player> _players;
        private List<Player> _fakePlayers;
        private List<int> _previousQuestions;
        private Question _currentQuestion;
        private Timer _nextQuestionTimer;
        private Timer _questionPauseTimer;
        private DateTime _timeStarted;
        private Random _random;
        private int _questionsAsked = 0;
        private int _numberOfQuestions;
        private int _questionLength;
        private int _pauseLengthBetweenQuestions;

        private static string[] FakeNames = 
        {
            "CalmYourself",
            "groveler",
            "Caducon",
            "Scriptaros",
            "Nitroria",
            "Cecilia",
            "Tartima",
            "Packave",
            "Mydor",
            "Vitage",
            "Calins",
            "Viaque",
            "Maniros",
            "Godsoft",
            "Archential",
            "Audibalon",
            "Origiwath",
            "Waphys",
            "Zengen",
            "Suncan",
            "Envitigue",
            "Epipace",
            "Locazonet",
            "Digipia",
            "Hotware",
            "Surage",
            "Hastre",
            "Epiten",
            "Norsite",
            "Patem",
            "Telmet",
            "Utocale",
            "Polywoct",
            "Labvan",
            "Mercuregue",
            "Aphesphan",
            "Ramaros",
            "Qerth",
            "Oxyson",
            "Tetranse",
            "Emperor",
            "Valus",
            "Technoboros",
            "Panist",
            "Apollia",
            "Portator",
            "Medisty",
            "Yucazos",
            "Tidescape",
            "Gigaxeme",
            "Touchica",
            "Mesotite",
            "Oraculove",
            "Sciege",
            "Spacyle",
            "Daylute",
            "Theonoge",
            "Intetate",
            "Genebol",
            "Futurupa",
            "Demomode",
            "Tense",
            "Wored",
            "Meltmalon",
            "Looza"
        };

        public delegate void NewQuestionHandler(object sender, QuestionEventArgs e);
        public event NewQuestionHandler NewQuestion;

        public delegate void GameOverHandler(object sender, GameOverEventArgs e);
        public event GameOverHandler GameOver;

        public delegate void QuestionFinishedHandler(object sender, QuestionFinishedEventArgs e);
        public event QuestionFinishedHandler QuestionFinished;

        public Game(IQuestionRepository questionRepo, int numberOfQuestions, int questionLength, int pauseLengthBetweenQuestions)
        {
            QuestionsAsked = new List<Question>();
            _questionRepo = questionRepo;
            Started = false;

            _numberOfQuestions = numberOfQuestions;
            _questionLength = questionLength;
            _pauseLengthBetweenQuestions = pauseLengthBetweenQuestions;

            _players = new Dictionary<string, Player>();
            _previousQuestions = new List<int>();
            _random = new Random();

            _fakePlayers = new List<Player>();
            for (int x = 0; x < _random.Next(7) + 2; x++)
            {
                _fakePlayers.Add(new Player(GenerateRandomPlayerName(), null));
            }
        }

        private string GenerateRandomPlayerName()
        {
            string randomName = null;

            for (int x = 0; x < FakeNames.Length && randomName == null; x++)
            {
                randomName = FakeNames[_random.Next(FakeNames.Length)];

                if (_fakePlayers.Any(p => p.PlayerName == randomName))
                {
                    randomName = null;
                }
            }

            if (randomName == null)
            {
                return "Looza";
            }

            return randomName;
        }

        public string GameCode { get; set; }

        public List<Question> QuestionsAsked { get; private set; }

        public bool Started { get; protected set; }

        public int NumberOfQuestions
        {
            get { return _numberOfQuestions; }
        }

        public void StartGame()
        {
            _timeStarted = DateTime.Now;

            if (_questionPauseTimer == null)
            {
                var duration = _pauseLengthBetweenQuestions * 1000;
                if (duration <= 0)
                {
                    duration = 1;
                }

                _questionPauseTimer = new Timer(duration);
                _questionPauseTimer.Elapsed += new ElapsedEventHandler(QuestionPauseTimer_Elapsed);
            }

            if (_nextQuestionTimer == null)
            {
                var duration = _questionLength * 1000;
                if (duration <= 0)
                {
                    duration = 1;
                }

                _nextQuestionTimer = new Timer(duration);
                _nextQuestionTimer.Elapsed += new System.Timers.ElapsedEventHandler(NextQuestionTimer_Elapsed);

                StartQuestionPause();

                Started = true;
            }
        }

        protected void QuestionPauseTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_questionPauseTimer != null)
            {
                _questionPauseTimer.Stop();
            }

            AskNextQuestion();

            if (_nextQuestionTimer != null)
            {
                _nextQuestionTimer.Start();
            }
        }

        public void AddPlayer(Player newPlayer, int maxPlayersAllowed)
        {
            if (!ContainsPlayer(newPlayer.ClientId))
            {
                _players.Add(newPlayer.ClientId, newPlayer);

                if (_players.Count + _fakePlayers.Count >= maxPlayersAllowed && _fakePlayers.Count > 0)
                {
                    int numFakePlayersToRemove = _players.Count + _fakePlayers.Count - maxPlayersAllowed;
                    if (numFakePlayersToRemove > _fakePlayers.Count)
                    {
                        numFakePlayersToRemove = _fakePlayers.Count;
                    }

                    _fakePlayers.RemoveRange(0, numFakePlayersToRemove);
                }
            }
        }

        public void RemovePlayer(string clientId)
        {
            if (ContainsPlayer(clientId))
            {
                _players.Remove(clientId);
            }
        }

        public void AnswerQuestion(string clientId, int answerNumber)
        {
            // 20% chance of Fake player answering before you
            if (_random.Next(5) == 0)
            {
                _currentQuestion.AnswerQuestion(_fakePlayers[0], _currentQuestion.Options.OrderBy(k => _random.Next()).First().Key);
            }

            var player = GetPlayer(clientId);

             if (player != null)
             {
                 _currentQuestion.AnswerQuestion(player, answerNumber);
             }
        }

        public int NumberOfPlayers()
        {
            return _players.Count;
        }

        public Player GetPlayer(string clientId)
        {
            Player player = null;
            _players.TryGetValue(clientId, out player);

            return player;
        }

        public bool ContainsPlayer(string clientId)
        {
            return _players.ContainsKey(clientId);
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public int EstimatedTimeTillNextQuestion(bool justJoined)
        {
            if (!Started)
            {
                // Game hasn't started, no idea when it will
                return 0;
            }

            if (_currentQuestion == null)
            {
                // Game has started, but no question has been asked yet
                double secondsSinceStarting = (DateTime.Now - _timeStarted).TotalSeconds;
                return (int)(_pauseLengthBetweenQuestions - secondsSinceStarting);
            }

            if (_currentQuestion.State == QuestionState.Asked)
            {
                // A question has been asked, find the current state
                double secondsSinceAsking = (DateTime.Now - _currentQuestion.TimeAsked).TotalSeconds;

                if (justJoined)
                {
                    secondsSinceAsking -= _pauseLengthBetweenQuestions;
                }

                return (int)(_questionLength - secondsSinceAsking);
            }

            if (_currentQuestion.State == QuestionState.None)
            {
                double secondsSinceCreating = (DateTime.Now - _currentQuestion.TimeCreated).TotalSeconds;
                return (int)(_pauseLengthBetweenQuestions - secondsSinceCreating);
            }

            if (_currentQuestion.State == QuestionState.Finished)
            {
                // time since finished?
                double secondsSinceFinished = (DateTime.Now - _currentQuestion.TimeFinished).TotalSeconds;
                return (int)(_pauseLengthBetweenQuestions - secondsSinceFinished);
            }

            return 0;
        }

        protected virtual void NextQuestionTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _nextQuestionTimer.Stop();

            if (_currentQuestion != null)
            {
                SetQuestionFinished();
            }

            if (_questionsAsked >= _numberOfQuestions)
            {
                SetGameOver();
                return;
            }

            StartQuestionPause();
        }

        private void StartQuestionPause()
        {
            if (_questionPauseTimer != null)
            {
                _questionPauseTimer.Start();
            }
        }

        private void SetQuestionFinished()
        {
            AddFakePlayerAnswers();

            _currentQuestion.Finished();

            if (QuestionFinished != null)
            {
                var unansweredPlayers = (from player in _players.Values
                                         where !_currentQuestion.CorrectPlayers.ContainsKey(player) && !_currentQuestion.IncorrectPlayers.Contains(player)
                                         select player).ToList();

                QuestionFinished(this, new QuestionFinishedEventArgs(_questionsAsked, _currentQuestion.CorrectPlayers, _currentQuestion.IncorrectPlayers, unansweredPlayers));
            }
        }

        private void AddFakePlayerAnswers()
        {
            foreach (var player in _fakePlayers)
            {
                _currentQuestion.AnswerQuestion(player, _currentQuestion.Options.OrderBy(k => _random.Next()).First().Key);
            }
        }

        private void SetGameOver()
        {
            if (GameOver != null)
            {
                GameOver(this, new GameOverEventArgs(_players.Select(kv => kv.Value).ToList()));
            }
        }

        private void AskNextQuestion()
        {
            _questionsAsked++;
            _currentQuestion = GetNextQuestion();
            _currentQuestion.QuestionAsked();

            QuestionsAsked.Add(_currentQuestion);

            if (NewQuestion != null)
            {
                NewQuestion(this, new QuestionEventArgs(_currentQuestion, _questionsAsked));
            }
        }

        private Question GetNextQuestion()
        {
            var newQuestion = _questionRepo.GetRandomQuestion(_previousQuestions);
            _previousQuestions.Add(newQuestion.Id);

            return newQuestion;
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_nextQuestionTimer != null)
                {
                    try
                    {
                        _nextQuestionTimer.Stop();
                    }
                    catch { }

                    try
                    {
                        _nextQuestionTimer.Dispose();
                    }
                    catch { }

                    _nextQuestionTimer = null;
                }

                if (_questionPauseTimer != null)
                {
                    try
                    {
                        _questionPauseTimer.Stop();
                    }
                    catch { }

                    try
                    {
                        _questionPauseTimer.Dispose();
                    }
                    catch { }

                    _questionPauseTimer = null;
                }
            }
        }

        ~Game()
        {
#if DEBUG
            //throw new ApplicationException("Object not disposed properly");
#endif
        }
    }
}