﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public enum QuestionState
    {
        None = 0,
        Asked = 1,
        Finished = 2
    }
}