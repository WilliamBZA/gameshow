﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public class GameOverEventArgs : EventArgs
    {
        public GameOverEventArgs(List<Player> players)
        {
            Players = players;
        }

        public List<Player> Players { get; set; }
    }
}