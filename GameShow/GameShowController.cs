﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gameshow.Exceptions;
using Gameshow.DataModel;

namespace Gameshow
{
    public class GameShowController
    {
        private IQuestionRepository _questionRepository;
        private Dictionary<string, Game> _games;
        private Dictionary<string, Player> _players;
        private int _maxPlayersPerGame = 25;
#if DEBUG
        private int _numberOfQuestionsPerRound = 2;
#else
        private int _numberOfQuestionsPerRound = 15;
#endif

#if DEBUG
        private int _pauseBetweenQuestions = 2;
#else
        private int _pauseBetweenQuestions = 7;
#endif
#if DEBUG
        private int _questionLength = 2;
#else
        private int _questionLength = 20;
#endif

        public GameShowController()
        {
            _games = new Dictionary<string, Game>();
            _players = new Dictionary<string, Player>();

            _questionRepository = new DatabaseQuestionRepository();
        }

        public Game CreateNewGame()
        {
            return CreateNewGame(GenerateGameCode());
        }

        private string GenerateGameCode()
        {
            return Guid.NewGuid().ToString();
        }

        public Game JoinGame(string clientId)
        {
            Game game = GetAvailableGame();

            lock (game)
            {
                System.Diagnostics.Debug.WriteLine("Doing join game on gamecode " + game.GameCode);

                JoinGame(game.GameCode, clientId);
            }

            return game;
        }

        public void JoinGame(string gameCode, string clientId)
        {
            Game game = GetGame(gameCode);
            if (game == null)
            {
                throw new GameNotFoundException("No game found with the specified gameCode");
            }

            Player player = GetPlayer(clientId);
            if (player == null)
            {
                throw new PlayerNotFoundException("No player found with the specified clientId");
            }

            if (!game.ContainsPlayer(player.ClientId))
            {
                game.AddPlayer(player, _maxPlayersPerGame);

                if (!game.Started)
                {
                    game.StartGame();
                }
            }
        }

        public Player GetPlayer(string clientId)
        {
            Player foundPlayer = null;
            _players.TryGetValue(clientId, out foundPlayer);

            return foundPlayer;
        }

        public Player GetPlayerFromClientId(string clientId)
        {
            return _players.Values.Where(player => player.ClientId == clientId).FirstOrDefault();
        }

        public IEnumerable<Player> AllPlayers()
        {
            return _players.Values;
        }

        public Game GetGame(string gameCode)
        {
            Game foundGame = null;
            _games.TryGetValue(gameCode, out foundGame);

            return foundGame;
        }

        public Game GetAvailableGame()
        {
            var possibleGames = from game in _games
                                where game.Value.NumberOfPlayers() < _maxPlayersPerGame
                                select game.Value;

            if (possibleGames.Count() == 0)
            {
                Game newGame = CreateNewGame();

                return newGame;
            }

            return possibleGames.FirstOrDefault();
        }

        public Player AddPlayer(string clientId, string playerName, string gravaterEmail)
        {
            if (!_players.ContainsKey(clientId))
            {
                Player newPlayer = new Player(playerName, gravaterEmail)
                {
                    ClientId = clientId
                };

                _players.Add(newPlayer.ClientId, newPlayer);

                return newPlayer;
            }
            else
            {
                return _players[clientId];
            }
        }

        public void RemovePlayer(string clientId)
        {
            Player foundPlayer = GetPlayer(clientId);

            if (foundPlayer != null)
            {
                RemovePlayerFromGame(foundPlayer);
                _players.Remove(foundPlayer.ClientId);
            }
        }

        private void RemovePlayerFromGame(Player player)
        {
            var game = GetPlayersCurrentGame(player.ClientId);
            if (game != null)
            {
                game.RemovePlayer(player.ClientId);
            }
        }

        public Game GetPlayersCurrentGame(string clientId)
        {
            var dictionaryItem = _games.FirstOrDefault(game => game.Value.ContainsPlayer(clientId));

            return dictionaryItem.Value;
        }

        public Game CreateNewGame(string gameCode)
        {
            var game = new Game(_questionRepository, _numberOfQuestionsPerRound, _questionLength, _pauseBetweenQuestions);
            game.GameCode = gameCode;

            game.GameOver += new Game.GameOverHandler(game_GameOver);

            _games.Add(game.GameCode, game);

            return game;
        }

        protected void game_GameOver(object sender, GameOverEventArgs e)
        {
            Game game = sender as Game;
            game.GameOver -= game_GameOver;
            
            _games.Remove(game.GameCode);

            game.Dispose();
        }

        public int NumberOfPlayers()
        {
            return _players.Count;
        }

        public int NumberOfGames()
        {
            return _games.Count;
        }
    }
}