﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow
{
    public interface IQuestionRepository
    {
        Question GetRandomQuestion(List<int> previouslyAskedQuestions);
    }
}