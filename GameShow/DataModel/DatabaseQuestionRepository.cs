﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Configuration;

namespace Gameshow.DataModel
{
    public class DatabaseQuestionRepository : IQuestionRepository
    {
        public Gameshow.Question GetRandomQuestion(List<int> previouslyAskedQuestions)
        {
            using (var context = new Gameshow.DataModel.GameshowDataModelDataContext(ConfigurationManager.ConnectionStrings["GameShow"].ConnectionString))
            {
                previouslyAskedQuestions = previouslyAskedQuestions ?? new List<int>();

                var loadOptions = new DataLoadOptions();
                loadOptions.LoadWith<Gameshow.DataModel.Question>(o => o.QuestionPossibilities);

                context.LoadOptions = loadOptions;

                var questions = from q in context.Questions
                                where !previouslyAskedQuestions.Contains(q.IdQuestion) && q.IsApproved
                                select q;

                int count = questions.Count() - 1;
                int index = new Random().Next(count);

                var question = questions.Skip(index).FirstOrDefault();

                return new Gameshow.Question()
                {
                    Id = question.IdQuestion,
                    QuestionText = question.QuestionText,
                    QuestionExplanation = question.Explanation,
                    Options = question.QuestionPossibilities.ToDictionary(qp => question.QuestionPossibilities.IndexOf(qp) + 1, qp => new QuestionOption { Text = qp.OptionText, IsCorrect = qp.IsCorrect })
                };
            }
        }

        public void AddError(string errorContent)
        {
            using (var context = new Gameshow.DataModel.GameshowDataModelDataContext(ConfigurationManager.ConnectionStrings["GameShow"].ConnectionString))
            {
                var error = new Error()
                {
                    Application = "Gameshow",
                    ErrorContent = errorContent
                };

                context.Errors.InsertOnSubmit(error);

                context.SubmitChanges();
            }
        }

        public void AddNewQuestion(string questionText, string questionDescription, string correctOption, string incorrectOption1, string incorrectOption2, string incorrectOption3)
        {
            AddNewQuestion(questionText, questionDescription, correctOption, incorrectOption1, incorrectOption2, incorrectOption3, null);
        }

        public void AddNewQuestion(string questionText, string questionDescription, string correctOption, string incorrectOption1, string incorrectOption2, string incorrectOption3, string referenceUrl)
        {
            if (string.IsNullOrWhiteSpace(referenceUrl))
            {
                referenceUrl = null;
            }

            using (var context = new Gameshow.DataModel.GameshowDataModelDataContext(ConfigurationManager.ConnectionStrings["GameShow"].ConnectionString))
            {
                var newQuestion = new Gameshow.DataModel.Question()
                {
                    Explanation = questionDescription,
                    QuestionText = questionText,
                    IsApproved = false,
                    ReferenceUrl = referenceUrl,
                    QuestionPossibilities = new EntitySet<QuestionPossibility>()
                    {
                        new QuestionPossibility()
                        {
                            IsCorrect = true,
                            OptionText = correctOption
                        },
                        new QuestionPossibility()
                        {
                            IsCorrect = false,
                            OptionText = incorrectOption1
                        },
                        new QuestionPossibility()
                        {
                            IsCorrect = false,
                            OptionText = incorrectOption2
                        },
                        new QuestionPossibility()
                        {
                            IsCorrect = false,
                            OptionText = incorrectOption3
                        }
                    }
                };

                context.Questions.InsertOnSubmit(newQuestion);
                context.SubmitChanges();
            }
        }
    }
}