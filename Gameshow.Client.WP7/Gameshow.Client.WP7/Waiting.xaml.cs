﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Gameshow.Client.WP7
{
    public partial class Waiting : UserControl
    {
        public Waiting()
        {
            InitializeComponent();
        }

        public void SetEstimateTimeLeft(int seconds)
        {
            CountdownClock.SetEstimateTimeLeft(seconds);
        }

        public void ShowHint(bool show)
        {
            if (show)
            {
                HelpfulHint.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                HelpfulHint.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}