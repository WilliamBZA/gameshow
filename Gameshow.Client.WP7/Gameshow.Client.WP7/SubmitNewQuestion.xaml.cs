﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SignalR.Client.Hubs;
using System.Threading.Tasks;

namespace Gameshow.Client.WP7
{
    public partial class SubmitNewQuestion : PhoneApplicationPage
    {
        public SubmitNewQuestion()
        {
            InitializeComponent();
        }

        private void AddQuestion_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(QuestionText.Text))
            {
                MessageBox.Show("You need to ask a question");
                return;
            }

            if (string.IsNullOrWhiteSpace(CorrectOption.Text))
            {
                MessageBox.Show("You need to provide a correct answer");
                return;
            }

            if (string.IsNullOrWhiteSpace(IncorrectOption1.Text) || string.IsNullOrWhiteSpace(IncorrectOption2.Text) || string.IsNullOrWhiteSpace(IncorrectOption3.Text))
            {
                MessageBox.Show("You need to provide 3 incorrect answers");
                return;
            }

            Processing.Visibility = System.Windows.Visibility.Visible;
            ContentPanel.Visibility = System.Windows.Visibility.Collapsed;

            SaveQuestion(QuestionText.Text, QuestionDescription.Text, CorrectOption.Text, IncorrectOption1.Text, IncorrectOption2.Text, IncorrectOption3.Text);
        }

        private void SaveQuestion(string questionText, string questionDescription, string correctOption, string incorrectOption1, string incorrectOption2, string incorrectOption3)
        {
            var added = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var gameConnection = new HubConnection(App.GameServerUrl);
                        var gameProxy = gameConnection.CreateProxy("Gameshow.Server.SignalR.GameShow");

                        var connected = gameConnection.Start().Wait(15000);

                        if (connected)
                        {
                            gameProxy.Invoke("AddQuestion", questionText, questionDescription, correctOption, incorrectOption1, incorrectOption2, incorrectOption3).Wait();

                            gameConnection.Stop();

                            QuestionAdded();
                        }
                        else
                        {
                            ErrorDuringConnection();
                        }
                    }
                    catch
                    {
                        ErrorDuringConnection();
                    }
                });
        }

        private void ErrorDuringConnection()
        {
            Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Looks like something went wrong! Please check your network and try join the game again.", "Oops!", MessageBoxButton.OK);

                    ContentPanel.Visibility = System.Windows.Visibility.Visible;
                    Processing.Visibility = System.Windows.Visibility.Collapsed;
                });
        }

        private void QuestionAdded()
        {
            Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Thanks, we've got it. Once we verify your question it'll be added to the list of questions in rotation.", "Thanks!", MessageBoxButton.OK);
                    
                    ContentPanel.Visibility = System.Windows.Visibility.Visible;
                    Processing.Visibility = System.Windows.Visibility.Collapsed;

                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }
                });
        }
    }
}