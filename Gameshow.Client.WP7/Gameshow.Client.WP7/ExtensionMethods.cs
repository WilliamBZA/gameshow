﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Gameshow.Client.WP7
{
    public static class ExtensionMethods
    {
        public static string AsOrdinalString(this int num)
        {
            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num.ToString() + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num.ToString() + "st";
                case 2:
                    return num.ToString() + "nd";
                case 3:
                    return num.ToString() + "rd";
                default:
                    return num.ToString() + "th";
            }
        }

        public static T GetParentOfType<T>(this DependencyObject item) where T : DependencyObject
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            var parent = VisualTreeHelper.GetParent(item);
            if (parent == null)
            {
                return null;
            }
            else if (parent.GetType().IsSubclassOf(typeof(T)))
            {
                return (T)parent;
            }
            else
            {
                return GetParentOfType<T>(parent);
            }
        }
    }
}
