﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using Gameshow.Client.WP7.Mogade;
using Mogade;
using Gameshow.Client.WP7.Gravatar;

namespace Gameshow.Client.WP7
{
    public class FinalScorePlayer
    {
        public string UserName { get; set; }

        public Uri GravatarUrl { get; set; }

        public DateTime Dated { get; set; }

        public int Score { get; set; }
    }

    public class AchievementItem
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Points { get; set; }

        public bool HasPlayerEarned { get; set; }
    }

    public partial class HighScores : PhoneApplicationPage
    {
        private bool _errorHandled;

        public HighScores()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            _errorHandled = false;

            LoadScores();
        }

        private void LoadScores()
        {
            LoadDailyLeaderboard();
            LoadWeeklyLeaderboard();
            LoadOverallLeaderboard();
            LoadAchievements();

            var store = IsolatedStorageSettings.ApplicationSettings;

            Dictionary<DateTime, decimal> scores;
            if (!store.TryGetValue("PlayerScores", out scores))
            {
                // No high scores yet
                DataContext = new string[] { "You haven't played any games yet!" };
            }
            else
            {
                DataContext = scores.OrderByDescending(kv => kv.Value).Select(kv => string.Format("{1}: {0:d}", kv.Key, kv.Value)).ToList();
            }

            int gamesPlayed;
            store.TryGetValue("TotalGamesPlayed", out gamesPlayed);
            decimal totalScore;
            store.TryGetValue("TotalScore", out totalScore);

            RunningTotal.DataContext = new RunningTotalWrapper
            {
                SummaryMessage = string.Format("You've played {0} games with a total score of {1}", gamesPlayed, totalScore),
                Ranking = GetRanking(gamesPlayed, totalScore)
            };
        }

        private void LoadDailyLeaderboard()
        {
            MogadeHelper.LoadLeaderboard(LeaderboardScope.Daily, 0, 25, r =>
            {
                if (r.Success)
                {
                    var bindingList = GetBindingList(r);

                    Dispatcher.BeginInvoke(() => 
                        {
                            DailyScoreList.ItemsSource = bindingList;
                            DailyScoreWaiting.Visibility = System.Windows.Visibility.Collapsed;
                        });
                }
                else
                {
                    
                }
            });
        }

        private void LeaderboardError()
        {
            if (!_errorHandled)
            {
                _errorHandled = true;
                Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Couldn't connect to the online leaderboard, perhaps you should try again.", "Oops", MessageBoxButton.OK);
                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }
                });
            }
        }

        private void LoadWeeklyLeaderboard()
        {
            MogadeHelper.LoadLeaderboard(LeaderboardScope.Weekly, 0, 25, r =>
            {
                if (r.Success)
                {
                    var bindingList = GetBindingList(r);

                    Dispatcher.BeginInvoke(() => 
                        {
                            WeeklyScoreList.ItemsSource = bindingList;
                            WeeklyScoreWaiting.Visibility = System.Windows.Visibility.Collapsed;
                        });
                }
                else
                {
                    LeaderboardError();
                }
            });
        }

        private void LoadOverallLeaderboard()
        {
            MogadeHelper.LoadLeaderboard(LeaderboardScope.Overall, 0, 25, r =>
            {
                if (r.Success)
                {
                    var bindingList = GetBindingList(r);
                    
                    Dispatcher.BeginInvoke(() => 
                        {
                            OverallScoreList.ItemsSource = bindingList;
                            OverallScoreWaiting.Visibility = System.Windows.Visibility.Collapsed;
                        });
                }
                else
                {
                    LeaderboardError();
                }
            });
        }

        private void LoadAchievements()
        {
            MogadeHelper.LoadAchievements(allAchievements =>
                {
                    if (allAchievements.Success)
                    {
                        
                        MogadeHelper.LoadEarnedAchievements((App.Current as App).WindowsLiveAnonymousID, r =>
                        {
                            if (r.Success)
                            {
                                var bindingList = from achievement in allAchievements.Data
                                                  join earned in r.Data on achievement.Id equals earned into playerEarned
                                                  from earned in playerEarned.DefaultIfEmpty()
                                                  select new AchievementItem
                                                  {
                                                      Description = achievement.Description,
                                                      Name = achievement.Name,
                                                      Points = achievement.Points,
                                                      HasPlayerEarned = !string.IsNullOrWhiteSpace(earned)
                                                  };

                                Dispatcher.BeginInvoke(() =>
                                {
                                    AchievementsEarned.ItemsSource = bindingList;
                                    AchievementsWaiting.Visibility = System.Windows.Visibility.Collapsed;
                                });
                            }
                            else
                            {
                                LeaderboardError();
                            }
                        });
                    }
                    else
                    {
                        LeaderboardError();
                    }
                });
        }

        private static IEnumerable<FinalScorePlayer> GetBindingList(Response<LeaderboardScores> r)
        {
            var bindingList = from score in r.Data.Scores
                              select new FinalScorePlayer
                              {
                                  UserName = score.UserName,
                                  GravatarUrl = new Uri(string.IsNullOrWhiteSpace(score.Data) ? "Images/GravatarDefault.jpg" : GravatarHelper.GetGravatarUri(score.Data, 75), string.IsNullOrWhiteSpace(score.Data) ? UriKind.Relative : UriKind.Absolute),
                                  Dated = score.Dated,
                                  Score = score.Points
                              };

            return bindingList;
        }

        private string GetRanking(int gamesPlayed, decimal totalScore)
        {
            if (gamesPlayed > 100)
            {
                return "Grand master";
            }
            else if (gamesPlayed > 70)
            {
                return "Junkie!";
            }
            else if (gamesPlayed > 50)
            {
                return "Serious Competitor";
            }
            else if (gamesPlayed > 30)
            {
                return "Local";
            }
            else if (gamesPlayed > 20)
            {
                return "Addict";
            }
            else if (gamesPlayed > 10)
            {
                return "Regular";
            }
            else if (gamesPlayed > 5)
            {
                return "Casual player";
            }

            return "Utter n00b";
        }
    }
}