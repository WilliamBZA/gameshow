﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;

namespace Gameshow.Client.WP7
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void joinPublicGameButton_Click(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Length == 0)
            {
                MessageBox.Show("You haven't chosen a username yet! Go to options first.");

                MainMenuPivot.SelectedIndex = 1;

                return;
            }

            (App.Current as App).Username = Username.Text;
            (App.Current as App).GravatarEmail = Gravatar.Text;

            NavigationService.Navigate(new Uri("/PlayGame.xaml", UriKind.Relative));
        }

        private void highScoresButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/HighScores.xaml", UriKind.Relative));
        }

        private void submitNewQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SubmitNewQuestion.xaml", UriKind.Relative));
        }

        private void aboutButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Gameshow.Client.WP7.About;component/About.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            EntranceStoryBoard.Begin();
            (App.Current as App).WindowsLiveAnonymousID = ExtendedPropertyHelper.GetWindowsLiveAnonymousID();

            var store = IsolatedStorageSettings.ApplicationSettings;

            string username;
            if (store.TryGetValue("Username", out username))
            {
                Dispatcher.BeginInvoke(() => Username.Text = username);
            }

            string gravatarEmail;
            if (store.TryGetValue("Gravatar", out gravatarEmail))
            {
                Dispatcher.BeginInvoke(() => Gravatar.Text = gravatarEmail);
            }

            string uniquePlayerId;
            if (!store.TryGetValue("UniquePlayerId", out uniquePlayerId))
            {
                uniquePlayerId = Guid.NewGuid().ToString();

                SaveValue(store, "UniquePlayerId", uniquePlayerId);
                store.Save();
            }

            (App.Current as App).UniquePlayerId = uniquePlayerId;

            Task.Factory.StartNew(() => (App.Current as App).ShowToasts());
        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {
            SaveUsername();
        }

        private void SaveUsername()
        {
            var store = IsolatedStorageSettings.ApplicationSettings;
            SaveValue(store, "Username", Username.Text);
            SaveValue(store, "Gravatar", Gravatar.Text);

            store.Save();
        }

        private void SaveValue(IsolatedStorageSettings store, string valueName, string value)
        {
            if (store.Contains(valueName))
            {
                store[valueName] = value;
            }
            else
            {
                store.Add(valueName, value);
            }
        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems != null && e.RemovedItems.Count > 0)
            {
                var item = e.RemovedItems[0] as PivotItem;
                if (item != null)
                {
                    if ((item.Header as string) == "options")
                    {
                        SaveUsername();
                    }
                }
            }
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("abbreviation of \"Globally Recognized Avatar\". It's an avatar that can be used practically anywhere! Gravatar is attached to your e-mail address so it follows you everywhere.  Register for your own at www.gravatar.com", "What is Gravatar?", MessageBoxButton.OK);
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            Dispatcher.BeginInvoke(() =>
                {
                    EntranceStoryBoard.Stop();
                });
        }
    }
}