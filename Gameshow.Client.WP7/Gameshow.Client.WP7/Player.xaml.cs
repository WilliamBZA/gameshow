﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using Gameshow.Client.WP7.Gravatar;
using System.Windows.Media.Imaging;
using Gameshow.Messages;

namespace Gameshow.Client.WP7
{
    public partial class Player : UserControl
    {
        public Player()
        {
            InitializeComponent();
        }

        public bool HideLastQuestionCorrectIcon { get; set; }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var gravatarUrl = (DataContext as PlayerWrapper).GravatarUrl();

            if (!string.IsNullOrEmpty(gravatarUrl))
            {
                Dispatcher.BeginInvoke(() => GravatarImage.Source = new BitmapImage(new Uri(gravatarUrl, UriKind.Absolute)));
            }

            if (HideLastQuestionCorrectIcon)
            {
                QuestionCorrectImage.Visibility = System.Windows.Visibility.Visible;
                CorrectStoryboard.Begin();
            }
        }
    }
}