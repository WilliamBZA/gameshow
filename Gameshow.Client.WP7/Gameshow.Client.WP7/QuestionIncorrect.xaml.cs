﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Gameshow.Client.WP7
{
    public partial class QuestionIncorrect : UserControl
    {
        private static string[] _responses = new string[]
        {
            "Too bad",
            "WTF?!",
            "Nope, sorry!",
            "Next time...",
            "Not even close!"
        };

        public QuestionIncorrect()
        {
            InitializeComponent();
        }

        public void ChangeWrongMessage()
        {
            Random rand = new Random();
            DataContext = _responses[rand.Next(_responses.Length)];
        }
    }
}