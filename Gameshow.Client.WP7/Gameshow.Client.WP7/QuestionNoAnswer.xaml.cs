﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Gameshow.Client.WP7
{
    public partial class QuestionNoAnswer : UserControl
    {
        private static string[] _responses = new string[]
        {
            "You didn't even try",
            "Come on, guess!",
            "A little slow there",
            "... you still there?",
            "Better safe than sorry?"
        };

        public QuestionNoAnswer()
        {
            InitializeComponent();
        }

        public void ChangeMissingMessage()
        {
            Random rand = new Random();
            DataContext = _responses[rand.Next(_responses.Length)];
        }
    }
}
