﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Gameshow.Client.WP7
{
    public class Question
    {
        public Question(int questionNumber, int totalQuestions, string questionTitle, string explanation, List<QuestionOption> options)
        {
            QuestionNumber = questionNumber;
            TotalQuestions = totalQuestions;
            QuestionTitle = questionTitle;
            Options = options;
            Explanation = explanation;
        }

        public int QuestionNumber { get; set; }

        public int TotalQuestions { get; set; }

        public string QuestionTitle { get; set; }

        public List<QuestionOption> Options { get; set; }

        public string Explanation { get; set; }

        public string ExplanationHeader
        {
            get
            {
                if (string.IsNullOrEmpty(Explanation))
                {
                    return null;
                }

                return "Info:";
            }
        }

        public override string ToString()
        {
            return string.Format("{0} out of {1}", QuestionNumber, TotalQuestions);
        }
    }
}