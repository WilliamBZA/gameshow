﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace Gameshow.Client.WP7.Gravatar
{
    public class GravatarHelper
    {
        public static string GetGravatarUri(string email, double width)
        {
            // Reference: http://en.gravatar.com/site/implement/url
            StringBuilder sb = new StringBuilder();

            sb.Append("http://www.gravatar.com/avatar/");
            sb.Append(Md5EncodeText(email ?? string.Empty));
            sb.Append(".jpg");

            // Size
            sb.Append("?s=");
            sb.Append((int)Math.Round(width));

            return sb.ToString();
        }

        protected static string Md5EncodeText(string text)
        {
            StringBuilder sb = new StringBuilder();

            byte[] ss = new MD5Managed().ComputeHash(Encoding.UTF8.GetBytes(text));

            foreach (byte b in ss)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString().ToLower();
        }
    }
}
