﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Gameshow.Messages;
using System.Threading.Tasks;

namespace Gameshow.Client.WP7
{
    public partial class QuestionComplete : UserControl
    {
        public QuestionComplete()
        {
            InitializeComponent();
        }

        public void SetGameOver(string summaryText)
        {
            GameOverSummary.Text = summaryText;
            FinalScores.Visibility = System.Windows.Visibility.Visible;
            NextQuestionClock.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void SetQuestionStatus(bool? wasCorrect, int timeTillNextQuestion)
        {
            NextQuestionClock.SetEstimateTimeLeft(timeTillNextQuestion);
            NextQuestionClock.Visibility = System.Windows.Visibility.Visible;

            if (wasCorrect == null)
            {
                Correct.Visibility = System.Windows.Visibility.Collapsed;
                Incorrect.Visibility = System.Windows.Visibility.Collapsed;
                NoAnswer.Visibility = System.Windows.Visibility.Visible;

                NoAnswer.ChangeMissingMessage();
            }
            else if (wasCorrect.Value)
            {
                Correct.Visibility = System.Windows.Visibility.Visible;
                Incorrect.Visibility = System.Windows.Visibility.Collapsed;
                NoAnswer.Visibility = System.Windows.Visibility.Collapsed;

                Correct.ChangeCongratsMessage();
            }
            else
            {
                Correct.Visibility = System.Windows.Visibility.Collapsed;
                Incorrect.Visibility = System.Windows.Visibility.Visible;
                NoAnswer.Visibility = System.Windows.Visibility.Collapsed;

                Incorrect.ChangeWrongMessage();
            }
        }

        private void FinalScores_Click(object sender, RoutedEventArgs e)
        {
            Correct.Visibility = System.Windows.Visibility.Collapsed;
            Incorrect.Visibility = System.Windows.Visibility.Collapsed;
            NoAnswer.Visibility = System.Windows.Visibility.Collapsed;
            FinalScores.Visibility = System.Windows.Visibility.Collapsed;
            PlayerScores.Visibility = System.Windows.Visibility.Collapsed;

            var orderedPlayers = from player in DataContext as List<PlayerWrapper>
                                 orderby player.Score descending
                                 select player;

            int x = 0;
            foreach (var player in orderedPlayers)
            {
                player.Order = x++;
            }

            GameOverScores.Visibility = System.Windows.Visibility.Visible;

            Task.Factory.StartNew(() => (App.Current as App).ShowToasts());
        }

        private void MainMenu_Click(object sender, RoutedEventArgs e)
        {
            var page = this.GetParentOfType<PhoneApplicationPage>();
            if (page.NavigationService.CanGoBack)
            {
                page.NavigationService.GoBack();
            }
        }
    }
}