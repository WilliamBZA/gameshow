﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Gameshow.Client.WP7
{
    public partial class ClockControl : UserControl
    {
        public ClockControl()
        {
            InitializeComponent();
        }

        public void SetEstimateTimeLeft(int seconds)
        {
            seconds = Math.Max(seconds, 1);
            if (SmallMode)
            {
                SmallProgressAnimation.Duration = new Duration(TimeSpan.FromSeconds(seconds + 1));
            }
            else
            {
                SecondHandAnimation.From = Math.Min(seconds * 30, 360);
                SecondHandAnimation.Duration = new Duration(TimeSpan.FromSeconds(seconds + 1));
            }
            CountdownAnimationStoryBoard.Begin();
        }

        private void ProgressAnimation_Completed(object sender, EventArgs e)
        {
            CountdownAnimationStoryBoard.Stop();
        }

        public bool SmallMode { get; set; }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (SmallMode)
            {
                SmallProgress.Visibility = System.Windows.Visibility.Visible;

                SecondHand.Visibility = System.Windows.Visibility.Collapsed;
                ClockFace.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                SmallProgress.Visibility = System.Windows.Visibility.Collapsed;

                SecondHand.Visibility = System.Windows.Visibility.Visible;
                ClockFace.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}
