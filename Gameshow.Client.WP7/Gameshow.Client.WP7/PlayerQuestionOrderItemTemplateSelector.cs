﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Gameshow.Messages;

namespace Gameshow.Client.WP7
{
    public class PlayerQuestionOrderItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FirstPlayer { get; set; }

        public DataTemplate SecondPlayer { get; set; }

        public DataTemplate ThirdPlayer { get; set; }

        public DataTemplate DefaultPlayer { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var player = item as PlayerWrapper;

            switch (player.Order)
            {
                case 0:
                    return FirstPlayer;

                case 1:
                    return SecondPlayer;

                case 2:
                    return ThirdPlayer;

                default:
                    return DefaultPlayer;
            }
        }
    }
}