﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;

namespace Gameshow.Client.WP7
{
    public class LittleWife
    {
        public static void Nag()
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("RunCount"))
            {
                IsolatedStorageSettings.ApplicationSettings["RunCount"] = 0;
            }

            int runCount = (int)IsolatedStorageSettings.ApplicationSettings["RunCount"];
            runCount++;

            if (runCount == 5)
            {
                runCount = 0;

                if (MessageBox.Show("Do you enjoy playing Multiplayer Quiz? Please take a quick minute to rate it...", "Multiplayer Quiz", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    MarketplaceReviewTask task = new MarketplaceReviewTask();
                    task.Show();
                }
            }
            IsolatedStorageSettings.ApplicationSettings["RunCount"] = runCount;

            IsolatedStorageSettings.ApplicationSettings.Save();
        }
    }
}
