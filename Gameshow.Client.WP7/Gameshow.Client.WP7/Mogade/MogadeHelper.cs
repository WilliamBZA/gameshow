﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Mogade.WindowsPhone;
using Mogade;

namespace Gameshow.Client.WP7.Mogade
{
    public enum Leaderboards
    {
        Main = 1,
    }

    public enum AchievementList
    {
        BabyBrain = 1,
        PerfectScore = 2,
        SuperBrain = 3
    }

    public class MogadeHelper
    {
        private const string _gameKey = "4eef15a4563d8a27560001ac";
        private const string _secret = "Mt_UrW^mQHxQDGohq_^hO]1XC";
        private static readonly IDictionary<Leaderboards, string> _leaderboardLookup = new Dictionary<Leaderboards, string>
                                                                                           {
                                                                                              { Leaderboards.Main, "4eef678a563d8a58b0000020" }
                                                                                           };
        private static readonly IDictionary<AchievementList, string> _achievementKeys = new Dictionary<AchievementList, string>
        {
            { AchievementList.BabyBrain, "4ef07d4a563d8a0f9100016b" },
            { AchievementList.PerfectScore, "4ef044c4563d8a0f9100007a" },
            { AchievementList.SuperBrain, "4ef04c72563d8a0d1900009d" }
        };


        private static IMogadeClient _mogadeInstance;

        private static string LeaderboardId(Leaderboards leaderboard)
        {
            return _leaderboardLookup[leaderboard];
        }

        private static IMogadeClient GetInstance()
        {
            if (_mogadeInstance == null)
            {
                MogadeConfiguration.Configuration(c => c.UsingUniqueIdStrategy(UniqueIdStrategy.UserId));
                _mogadeInstance = MogadeClient.Initialize(_gameKey, _secret);
                _mogadeInstance.LogApplicationStart();
            }

            return _mogadeInstance;
        }

        public static void SaveScore(string username, string gravaterEmail, int score, Action<Response<SavedScore>> scoreResponseHandler)
        {
            var scoreObject = new Score
            {
                Data = gravaterEmail,
                Points = score,
                UserName = username 
            };

            GetInstance().SaveScore(MogadeHelper.LeaderboardId(Leaderboards.Main), scoreObject, scoreResponseHandler);
        }

        public static void LoadLeaderboard(LeaderboardScope scope, int page, int pageSize, Action<Response<LeaderboardScores>> callback)
        {
            GetInstance().GetLeaderboard(MogadeHelper.LeaderboardId(Leaderboards.Main), scope, page, pageSize, result => callback(result));
        }

        public static void AddAchievement(AchievementList achievement, string username, Action<Response<Achievement>> callback)
        {
            GetInstance().AchievementEarned(_achievementKeys[achievement], username, callback);
        }

        public static void LoadAchievements(Action<Response<ICollection<Achievement>>> callback)
        {
            GetInstance().GetAchievements(callback);
        }

        public static void LoadEarnedAchievements(string username, Action<Response<ICollection<string>>> callback)
        {
            GetInstance().GetEarnedAchievements(username, callback);
        }
    }
}