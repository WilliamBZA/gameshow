﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Info;
using Coding4Fun.Phone.Controls;
using System.Threading;
using System.Windows.Threading;

namespace Gameshow.Client.WP7
{
    public partial class App : Application
    {
        public const string GameServerUrl = "http://www.williamb.net";
        //public const string GameServerUrl = "http://localhost:5282/";
        //public const string GameServerUrl = "http://MultiplayerQuiz.apphb.com/";

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                //MetroGridHelper.IsVisible = true;

                // Display the current frame rate counters.
                //Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;
            }

            PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;

        }

        public string WindowsLiveAnonymousID { get; set; }

        public string Username { get; set; }

        public string GravatarEmail { get; set; }

        public string UniquePlayerId { get; set; }

        public Queue<ToastPrompt> ToastsToShow { get; private set; }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            LittleWife.Nag();
            ToastsToShow = new Queue<ToastPrompt>();
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            ToastsToShow = new Queue<ToastPrompt>();
        }

        public void ShowToasts()
        {
            ManualResetEvent toastFinished = new ManualResetEvent(false);

            while (ToastsToShow.Count > 0)
            {
                var toast = ToastsToShow.Dequeue();
                toast.Completed += (sender, e) =>
                    {
                        toastFinished.Set();
                    };

                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => 
                    {
                        toast.Show();
                    });
                toastFinished.WaitOne(4000);
            }
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            LittleWatson.ReportException(e.ExceptionObject, string.Empty);

            e.Handled = true;

            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        private WebException FindWebExceptionIn(AggregateException aggregateException)
        {
            if (aggregateException.InnerExceptions == null || aggregateException.InnerExceptions.Count == 0)
            {
                return null;
            }

            foreach (var ex in aggregateException.InnerExceptions)
            {
                if (ex.InnerException is WebException)
                {
                    return ex.InnerException as WebException;
                }

                if (ex.InnerException is AggregateException)
                {
                    var webException = FindWebExceptionIn(ex.InnerException as AggregateException);
                    if (webException != null)
                    {
                        return webException;
                    }
                }
            }

            return null;
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}