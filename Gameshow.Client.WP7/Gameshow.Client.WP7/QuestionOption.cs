﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gameshow.Client.WP7
{
    public class QuestionOption
    {
        public string Key { get; set; }

        public string Label { get; set; }
    }
}