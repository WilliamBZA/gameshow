﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SignalR.Client.Hubs;
using SignalR.Client;
using System.Threading.Tasks;
using System.Threading;
using System.IO.IsolatedStorage;
using Gameshow.Messages;
using Gameshow.Client.WP7.Mogade;
using Coding4Fun.Phone.Controls;
using System.Windows.Media.Imaging;
using Mogade;
using Microsoft.Phone.Tasks;

namespace Gameshow.Client.WP7
{
    public partial class PlayGame : PhoneApplicationPage
    {
        private int _questionNumber = 0;
        private string _gameCode;
        private string _playerName;
        private string _gravatarEmail;
        private string _clientId;
        private IHubProxy _gameProxy;
        private HubConnection _gameConnection;
        private bool _questionAsked = false;
        private bool _wasFirstInAllQuestions = true;
        private bool _joinCancelled = false;
        private List<PlayerWrapper> _lastScores;
        private Question _question;
        private string _gameServerUrl;
        private bool _questionWrong;

        public PlayGame(string gameCode)
        {
            _gameCode = gameCode;

            InitializeComponent();
        }

        public PlayGame()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            _questionWrong = false;
            _gameServerUrl = App.GameServerUrl;
            _playerName = (App.Current as App).Username;
            _gravatarEmail = (App.Current as App).GravatarEmail;

            if (string.IsNullOrEmpty(_gameCode))
            {
                JoinPublicGame();
            }
            else
            {
                JoinPrivateGame(_gameCode);
            }
        }

        private void JoinPrivateGame(string gameCode)
        {
            throw new NotImplementedException();
        }

        private void InitializeConnection()
        {
            _gameConnection = new HubConnection(_gameServerUrl);

            ManualResetEvent waitForOk = new ManualResetEvent(false);

            _gameProxy = _gameConnection.CreateProxy("Gameshow.Server.SignalR.GameShow");

            _gameProxy.On("CarryOnThen", () =>
                {
                    if (waitForOk != null)
                    {
                        waitForOk.Set();
                    }
                });

            _gameConnection.Error += new Action<Exception>(_gameConnection_Error);
            _gameConnection.Closed += new Action(_gameConnection_Closed);

            _gameProxy.On<string>("ReceiveMessage", (message) =>
            {
                MessageBox.Show(message);
            });

            _gameProxy.On<NewQuestion>("NewQuestion", NewQuestion);
            _gameProxy.On<List<KeyValuePair<string, string>>>("GameOver", GameOver);
            _gameProxy.On<QuestionFinished>("QuestionCompleted", QuestionCompleted);

            SetTitleMessage("Trying to find an open game...");

            if (!_gameConnection.Start().Wait(60000))
            {
                throw new Exception("Couldn't connect");
            }

            PlayerJoinedResult joinedResult = null;
            Task<PlayerJoinedResult> joinTask = null;
            bool connectionSuccessful = false;
            do
            {
                joinTask = _gameProxy.Invoke<PlayerJoinedResult>("JoinGame", _playerName, _gravatarEmail);
                joinTask.Wait();

                connectionSuccessful = waitForOk.WaitOne(5000);
                
                if (joinTask.IsCompleted)
                {
                    joinedResult = joinTask.Result;

                    if (!string.IsNullOrWhiteSpace(joinedResult.RedirectionUrl))
                    {
                        // Server has potentially moved
                        if (string.Compare(_gameServerUrl, joinedResult.RedirectionUrl, StringComparison.InvariantCultureIgnoreCase) != 0)
                        {
                            // Redirect
                            _gameServerUrl = joinedResult.RedirectionUrl;
                            _gameProxy = null;
                            _gameConnection.Stop();
                            _gameConnection = null;

                            InitializeConnection();

                            return;
                        }
                    }
                }
            } while (!connectionSuccessful  && !_joinCancelled);

            SendLittleWatson();

            if (joinedResult.Version != "1.2.0.0")
            {
                Dispatcher.BeginInvoke(() =>
                    {
                        if (MessageBox.Show("An update is available. We recommend you install the update before trying to play as some of the features may not work anymore.", "An update is available!", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                        {
                            var task = new MarketplaceDetailTask();
                            task.Show();

                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        }
                    });
            }

            _clientId = joinedResult.ClientId;

            int secondsToWait = joinedResult.EstimatedSecondsTillNextQuestion;
            SetTitleMessage("Waiting for next question...");
            Dispatcher.BeginInvoke(() =>
            {
                WaitingControl.ShowHint(true);
                WaitingControl.SetEstimateTimeLeft(secondsToWait);
                ContentPanel.Visibility = System.Windows.Visibility.Collapsed;
                WaitingControl.Visibility = System.Windows.Visibility.Visible;
                progressBar.Visibility = System.Windows.Visibility.Collapsed;
            });
        }

        private void SendLittleWatson()
        {
            var previousErrors = LittleWatson.CheckForPreviousException();

            if (!string.IsNullOrWhiteSpace(previousErrors))
            {
                _gameProxy.Invoke("PreviousErrors", previousErrors);
            }
        }

        void _gameConnection_Closed()
        {
        }

        void _gameConnection_Error(Exception obj)
        {
        }

        protected void GameOver(List<KeyValuePair<string, string>> playersAndScores)
        {
            var player = _lastScores.FirstOrDefault(p => p.ClientId == _clientId);
            var orderedScored = _lastScores.OrderByDescending(p => p.Score).ToList();

            var playerCame = orderedScored.IndexOf(player) + 1;

            SavePlayersScore(player.Score, _questionWrong, _wasFirstInAllQuestions);

            string message = null;
            if (playerCame <= _lastScores.Count / 2)
            {
                message = string.Format("Congratulations! You came {0}!", playerCame.AsOrdinalString());
            }
            else
            {
                message = string.Format("You came {0}.", playerCame.AsOrdinalString());
            }

            Dispatcher.BeginInvoke(() => 
                {
                    ApplicationTitle.Text = "Game over!";
                    QuestionComplete.SetGameOver(message);
                });

            LeaveGame();
        }

        private void SavePlayersScore(decimal score, bool gotAnyQuestionsWrong, bool wasPlayerFirstCorrectAnswer)
        {
            MogadeHelper.SaveScore((App.Current as App).Username, (App.Current as App).GravatarEmail, (int)score, null);

            MogadeHelper.AddAchievement(AchievementList.BabyBrain, (App.Current as App).WindowsLiveAnonymousID, r => CheckAchievement(r));

            if (!gotAnyQuestionsWrong)
            {
                MogadeHelper.AddAchievement(AchievementList.PerfectScore, (App.Current as App).WindowsLiveAnonymousID, r => CheckAchievement(r));
            }

            if (wasPlayerFirstCorrectAnswer)
            {
                MogadeHelper.AddAchievement(AchievementList.SuperBrain, (App.Current as App).WindowsLiveAnonymousID, r => CheckAchievement(r));
            }

            var store = IsolatedStorageSettings.ApplicationSettings;

            var previousScores = GetPreviousPlayerScores(store);
            previousScores.Add(DateTime.Now, score);

            if (previousScores.Count > 3)
            {
                previousScores.Remove(previousScores.OrderBy(kv => kv.Value).First().Key);
            }

            IncreaseRunningTotal(store, score);

            store["PlayerScores"] = previousScores;
            store.Save();
        }

        private void CheckAchievement(Response<Achievement> r)
        {
            if (r.Success && r.Data.Points > 0)
            {
                AchievementUnlocked(r.Data.Name, r.Data.Description, r.Data.Points);
            }
        }

        private void AchievementUnlocked(string achievementName, string achievementDescription, int achievementPoints)
        {
            Dispatcher.BeginInvoke(() =>
                {
                    var toast = new ToastPrompt
                    {
                        Title = "Achivement Unlocked",
                        TextOrientation = System.Windows.Controls.Orientation.Vertical,
                        Message = string.Format("{0} - {2} points.{3}{1}", achievementName, achievementDescription, achievementPoints, Environment.NewLine),
                        ImageSource = new BitmapImage(new Uri("/Images/Trophy.png", UriKind.Relative))
                    };

                    (App.Current as App).ToastsToShow.Enqueue(toast);
                });
        }

        private void IncreaseRunningTotal(IsolatedStorageSettings store, decimal score)
        {
            if (store.Contains("TotalGamesPlayed"))
            {
                store["TotalGamesPlayed"] = (int)store["TotalGamesPlayed"] + 1;
            }
            else
            {
                store.Add("TotalGamesPlayed", 1);
            }

            if (store.Contains("TotalScore"))
            {
                store["TotalScore"] = (decimal)store["TotalScore"] + score;
            }
            else
            {
                store.Add("TotalScore", score);
            }
        }

        private Dictionary<DateTime, decimal> GetPreviousPlayerScores(IsolatedStorageSettings store)
        {
            Dictionary<DateTime, decimal> scores;
            if (!store.TryGetValue("PlayerScores", out scores))
            {
                scores = new Dictionary<DateTime, decimal>();
                store.Add("PlayerScores", scores);
            }

            return scores;
        }

        protected void NewQuestion(NewQuestion newQuestion)
        {
            _questionAsked = true;
            _questionNumber++;

            SetTitleMessage("QUESTION");
            
            var random = new Random();
            _question = new Question(newQuestion.QuestionNumber, newQuestion.TotalQuestions, newQuestion.QuestionTitle, newQuestion.QuestionExplanation, newQuestion.PossibleAnswers.OrderBy(kv => random.Next()).Select(kv => new QuestionOption() { Key = kv.Key, Label = kv.Value }).ToList());

            HideAllControlsExcept(ContentPanel, 
                () => 
                {
                    DataContext = _question;
                    TimerClock.SetEstimateTimeLeft(newQuestion.Duration);
                });
        }

        protected void OptionSelected(object sender, RoutedEventArgs e)
        {
            var answerNumber = (sender as Button).Tag as string;
            Task.Factory.StartNew(() => AnswerQuestion(answerNumber));
        }

        private void AnswerQuestion(string answerNumber)
        {
            if (_gameProxy != null)
            {
                HideAllControlsExcept(BusyIcon, null);

                bool answered = false;
                int answerAttempt = 0;

                Task<AnswerQuestionResult> answerTask = null;
                while (!answered && answerAttempt <= 4)
                {
                    answerAttempt++;

                    try
                    {
                        answerTask = _gameProxy.Invoke<AnswerQuestionResult>("AnswerQuestion", answerNumber);
                        answered = answerTask.Wait(2000);
                    }
                    catch { }
                }

                if (answered && answerTask != null)
                {
                    SetQuestionAnswered(answerTask.Result.EstimatedSecondsTillQuestionEnd);

                    SetTitleMessage("Waiting for the slow guys to answer...");
                }
                else
                {
                    // Something went wrong
                    Dispatcher.BeginInvoke(() =>
                        {
                            MessageBox.Show("Looks like something went wrong! Please check your network and try join the game again.", "Oops!", MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        });
                }
            }
        }

        private void SetQuestionAnswered(int waitDurationSeconds)
        {
            HideAllControlsExcept(WaitingControl,
                () =>
                {
                    WaitingControl.ShowHint(false);
                    WaitingControl.DataContext = _question;
                    WaitingControl.SetEstimateTimeLeft(waitDurationSeconds);
                });
        }

        protected void QuestionCompleted(QuestionFinished questionResult)
        {
            if (_questionAsked)
            {
                var playerScores = questionResult.CorrectPlayers.Union(questionResult.IncorrectPlayers).Union(questionResult.UnansweredPlayers).OrderByDescending(kv => kv.Score).ToList();
                var message = "You didn't even want to guess?";
                bool? wasCorrect = null;

                if (questionResult.CorrectPlayers.Any(p => p.ClientId == _clientId))
                {
                    wasCorrect = true;
                    message = "You got it right!!!";

                    if (questionResult.CorrectPlayers.FirstOrDefault(p => p.ClientId == _clientId).Order != 0)
                    {
                        _wasFirstInAllQuestions = false;
                    }
                }
                else if (questionResult.IncorrectPlayers.Any(p => p.ClientId == _clientId))
                {
                    _questionWrong = true;
                    _wasFirstInAllQuestions = false;
                    wasCorrect = false;
                    message = "You got it wrong :(";
                }
                else
                {
                    // didn't answer, counts as wrong
                    _questionWrong = true;
                    _wasFirstInAllQuestions = false;
                }

                HideAllControlsExcept(QuestionComplete,
                        () =>
                        {
                            QuestionComplete.DataContext = playerScores;
                            QuestionComplete.SetQuestionStatus(wasCorrect, questionResult.EstimatedTimeTillNextQuestion);
                            ApplicationTitle.Text = message;
                        });

                _lastScores = playerScores;
            }

            _questionAsked = false;
        }

        protected void HideAllControlsExcept(FrameworkElement controlToShow, Action alsoDo)
        {
            Dispatcher.BeginInvoke(() =>
                {
                    BusyIcon.Visibility = System.Windows.Visibility.Collapsed;
                    WaitingControl.Visibility = System.Windows.Visibility.Collapsed;
                    QuestionComplete.Visibility = System.Windows.Visibility.Collapsed;
                    ContentPanel.Visibility = System.Windows.Visibility.Collapsed;

                    if (alsoDo != null)
                    {
                        alsoDo();
                    }

                    controlToShow.Visibility = System.Windows.Visibility.Visible;
                });
        }

        private void SetTitleMessage(string message)
        {
            Dispatcher.BeginInvoke(() => ApplicationTitle.Text = message);
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            _joinCancelled = true;

            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            _joinCancelled = true;

            base.OnNavigatingFrom(e);

            LeaveGame();
        }

        private void LeaveGame()
        {
            _joinCancelled = true;
            Task.Factory.StartNew(() =>
            {
                _gameProxy.Invoke("LeaveGame").Wait();
                _gameConnection.Stop();
            });
        }

        private void JoinPublicGame()
        {
            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        InitializeConnection();
                    }
                    catch (Exception ex)
                    {
                        string val = ex.ToString();

                        Dispatcher.BeginInvoke(() =>
                            {
                                MessageBox.Show("I couldn't find the server for some reason. Maybe you should try again?");
                                if (NavigationService.CanGoBack)
                                {
                                    NavigationService.GoBack();
                                }
                            });
                    }
                });
        }
    }
}