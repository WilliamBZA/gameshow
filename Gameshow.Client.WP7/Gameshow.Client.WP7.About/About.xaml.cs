﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Tasks;
using System.IO;
using System.Windows.Resources;

namespace Gameshow.Client.WP7.About
{
    public partial class About : PhoneApplicationPage
    {
        private StackPanel _whatsNew;

        public About()
        {
            InitializeComponent();
        }

        private void ReviewButton_Click(object sender, RoutedEventArgs e)
        {
            string s = ((ButtonBase)sender).Tag as string;

            switch (s)
            {
                case "Review":
                    var task = new MarketplaceReviewTask();
                    task.Show();
                    break;
            }
        }

        private void Pivot_Loaded(object sender, RoutedEventArgs e)
        {
            Uri manifest = new Uri("WMAppManifest.xml", UriKind.Relative);
            var si = Application.GetResourceStream(manifest);
            if (si != null)
            {
                using (StreamReader sr = new StreamReader(si.Stream))
                {
                    bool haveApp = false;
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (!haveApp)
                        {
                            int i = line.IndexOf("AppPlatformVersion=\"", StringComparison.InvariantCulture);
                            if (i >= 0)
                            {
                                haveApp = true;
                                line = line.Substring(i + 20);

                                int z = line.IndexOf("\"");
                                if (z >= 0)
                                {
                                    // if you're interested in the app plat version at all
                                    // AppPlatformVersion = line.Substring(0, z);
                                }
                            }
                        }

                        int y = line.IndexOf("Version=\"", StringComparison.InvariantCulture);
                        if (y >= 0)
                        {
                            int z = line.IndexOf("\"", y + 9, StringComparison.InvariantCulture);
                            if (z >= 0)
                            {
                                // We have the version, no need to read on.
                                _versionText.Text = line.Substring(y + 9, z - y - 9);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                _versionText.Text = "Unknown";
            }
        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Pivot piv = (Pivot)sender;
            if (piv.SelectedIndex > 0 && _whatsNew == null)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    _whatsNew = new StackPanel();

                    StreamResourceInfo sri = Application.GetResourceStream(new Uri("WHATSNEW.txt", UriKind.Relative));
                    if (sri != null)
                    {
                        using (StreamReader sr = new StreamReader(sri.Stream))
                        {
                            string line;
                            bool lastWasEmpty = true;
                            do
                            {
                                line = sr.ReadLine();

                                if (string.IsNullOrWhiteSpace(line))
                                {
                                    Rectangle r = new Rectangle
                                    {
                                        Height = 20,
                                    };
                                    _whatsNew.Children.Add(r);
                                    lastWasEmpty = true;
                                }
                                else
                                {
                                    TextBlock tb = new TextBlock
                                    {
                                        TextWrapping = TextWrapping.Wrap,
                                        Text = line,
                                        Style = (Style)Application.Current.Resources["PhoneTextNormalStyle"],
                                    };
                                    if (!lastWasEmpty)
                                    {
                                        tb.Opacity = 0.7;
                                    }
                                    lastWasEmpty = false;
                                    _whatsNew.Children.Add(tb);
                                }
                            } while (line != null);
                        }
                    }

                    whatsNewViewer.Content = _whatsNew;
                });
            }
        }
    }
}