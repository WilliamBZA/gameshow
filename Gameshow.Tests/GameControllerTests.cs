﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using FluentAssertions;
using Gameshow;

namespace GameShow.Tests
{
    public class GameShowControllerTests
    {
        public const string TestPlayerName = "WilliamBZA_Test";

        GameShowController controller;

        public GameShowControllerTests()
        {
            controller = new GameShowController();
        }

        [Fact]
        public void Adding_New_Player_AddsPlayerObject_ToPlayerList()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();

            // Act
            Player newPlayer = controller.AddPlayer(clientId, TestPlayerName, null);

            // Assert
            newPlayer.ClientId.Should().Be(clientId);
            newPlayer.PlayerName.Should().Be(TestPlayerName);

            Player loadedPlayer = controller.GetPlayer(newPlayer.ClientId);
            loadedPlayer.Should().Be(newPlayer);
        }

        [Fact]
        public void Adding_Existing_Player_RemovesPlayerFrom_ExistingGames()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player newPlayer = controller.AddPlayer(clientId, TestPlayerName, null);
            controller.JoinGame(newPlayer.ClientId);

            // Act
            Player duplicatedPlayer = controller.AddPlayer(newPlayer.ClientId, newPlayer.PlayerName, null);
            Game playerGame = controller.GetPlayersCurrentGame(duplicatedPlayer.ClientId);

            // Assert
            playerGame.Should().BeNull();
        }

        [Fact]
        public void Adding_APlayer_ToAGame_MeansThePlayersGameCanBeFound()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player newPlayer = controller.AddPlayer(clientId, TestPlayerName, null);

            // Act
            controller.JoinGame(newPlayer.ClientId);

            // Assert
            var game = controller.GetPlayersCurrentGame(newPlayer.ClientId);

            game.Should().NotBeNull();
        }

        [Fact]
        public void JoiningAnExistingGame_Sets_PlayersGameCorrectly()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player newPlayer = controller.AddPlayer(clientId, TestPlayerName, null);

            string gameCode = "TESTGAMECODE";
            var newGame = controller.CreateNewGame(gameCode);

            // Act
            controller.JoinGame(gameCode, newPlayer.ClientId);

            // Assert
            var game = controller.GetPlayersCurrentGame(newPlayer.ClientId);
            game.GameCode.Should().Be(gameCode);

            newGame.Should().Be(game);
        }
    }
}