﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using FluentAssertions;
using Gameshow;

namespace GameShow.Tests
{
    public class QuestionTests
    {
        [Fact]
        public void Answering_A_Question_Correctly_Increases_Players_Score()
        {
            // Arrange
            Question question = new Question();
            question.Options.Add(1, new QuestionOption() { Text = "Correct", IsCorrect=true });
            question.Options.Add(2, new QuestionOption() { Text = "Incorrect", IsCorrect=false });

            int initialScore = 0;

            Player player = new Player("Test", null)
            {
                PlayerName = "Test",
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };

            // Act
            question.AnswerQuestion(player, 1);

            // Assert
            player.Score.Should().BeGreaterThan(initialScore + 100);
        }

        [Fact]
        public void Being_The_First_Player_To_Answer_Correctly_Means_Bonus_Points()
        {
            // Arrange
            Question question = new Question();
            question.Options.Add(1, new QuestionOption() { Text = "Correct", IsCorrect = true });
            question.Options.Add(2, new QuestionOption() { Text = "Incorrect", IsCorrect = false });

            int initialScore = 0;

            Player player1 = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };
            Player player2 = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };
            Player player3 = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };
            Player player4 = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };

            // Act
            question.AnswerQuestion(player1, 1);
            question.AnswerQuestion(player2, 1);
            question.AnswerQuestion(player3, 1);
            question.AnswerQuestion(player4, 1);

            // Assert
            player4.Score.Should().Be(initialScore + 100);
            player3.Score.Should().Be(initialScore + 100 + 100);
            player2.Score.Should().Be(initialScore + 100 + 2*100);
            player1.Score.Should().Be(initialScore + 100 + 3*100);
        }

        [Fact]
        public void Answering_Question_Incorrectly_Doesnt_Impact_PlayerScore()
        {
            // Arrange
            Question question = new Question();
            question.Options.Add(1, new QuestionOption() { Text = "Correct", IsCorrect = true });
            question.Options.Add(2, new QuestionOption() { Text = "Incorrect", IsCorrect = false });

            int initialScore = 0;

            Player player1 = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };
            Player player2 = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };

            // Act
            question.AnswerQuestion(player1, 2);
            question.AnswerQuestion(player2, 1);

            // Assert
            player1.Score.Should().Be(initialScore);
            player2.Score.Should().Be(initialScore + 100 + 3*100);
        }

        [Fact]
        public void Answering_Question_Multiple_Times_Doesnt_Change_Score()
        {
            // Arrange
            Question question = new Question();
            question.Options.Add(1, new QuestionOption() { Text = "Correct", IsCorrect = true });
            question.Options.Add(2, new QuestionOption() { Text = "Incorrect", IsCorrect = false });

            int initialScore = 0;

            Player player = new Player("Test", null)
            {
                ClientId = Guid.NewGuid().ToString(),
                Score = initialScore
            };

            // Act
            question.AnswerQuestion(player, 1);
            question.AnswerQuestion(player, 1);
            question.AnswerQuestion(player, 1);
            question.AnswerQuestion(player, 1);
            question.AnswerQuestion(player, 1);
            question.AnswerQuestion(player, 2);
            question.AnswerQuestion(player, 2);

            // Assert
            player.Score.Should().Be(initialScore + 100 + 3*100);
        }
    }
}