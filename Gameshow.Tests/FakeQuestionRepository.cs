﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gameshow;

namespace GameShow.Tests
{
    public class FakeQuestionRepository : IQuestionRepository
    {
        public Question GetRandomQuestion(List<int> previouslyAskedQuestions)
        {
            return new Question()
            {
                QuestionText = "How awesome is WilliamBZA?",
                Options = new Dictionary<int, QuestionOption>()
                {
                    {1, new QuestionOption() { Text = "So so" } },
                    {2, new QuestionOption() { Text = "Woah, he's cool" } },
                    {3, new QuestionOption() { Text = "Super cool" } },
                    {4, new QuestionOption() { Text = "THE BEST EVA", IsCorrect = true } }
                }
            };
        }
    }
}