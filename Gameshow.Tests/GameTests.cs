﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using FluentAssertions;
using System.Threading;
using Gameshow;

namespace GameShow.Tests
{
    public class GameTests
    {
        [Fact]
        public void CheckingForAddedPlayer_Returns_True_For_IfPlayerAdded()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player player = new Player(GameShowControllerTests.TestPlayerName, null)
            {
                ClientId = clientId
            };

            using (var game = new Game(new FakeQuestionRepository(), 1, 0, 0) { GameCode = "TEST GAME" })
            {
                game.AddPlayer(player, 99);

                // Act
                bool contains = game.ContainsPlayer(player.ClientId);

                // Assert
                contains.Should().BeTrue();
                game.NumberOfPlayers().Should().Be(1);
            }
        }

        [Fact]
        public void AddingSamePlayerTwice_Does_Nothing()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player player = new Player(GameShowControllerTests.TestPlayerName, null)
            {
                ClientId = clientId
            };

            using (var game = new Game(new FakeQuestionRepository(), 1, 0, 0) { GameCode = "TEST GAME" })
            {
                game.AddPlayer(player, 99);

                Player secondPlayer = new Player(GameShowControllerTests.TestPlayerName, null)
                {
                    ClientId = clientId
                };

                // Act
                game.AddPlayer(secondPlayer, 99);

                // Assert
                game.NumberOfPlayers().Should().Be(1);
            }
        }

        [Fact]
        public void RemovingPlayer_Deletes_Player_FromList()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player player = new Player(GameShowControllerTests.TestPlayerName, null)
            {
                ClientId = clientId
            };

            using (var game = new Game(new FakeQuestionRepository(), 1, 0, 0) { GameCode = "TEST GAME" })
            {
                game.AddPlayer(player, 99);

                // Act
                game.RemovePlayer(player.ClientId);

                // Assert
                game.ContainsPlayer(player.ClientId).Should().BeFalse();
                game.NumberOfPlayers().Should().Be(0);
            }
        }

        [Fact]
        public void AddingMultiplePlayers_Increases_PlayerCount()
        {
            // Arrange
            string clientId = Guid.NewGuid().ToString();
            Player player = new Player("Test", null)
            {
                ClientId = clientId,
                PlayerName = GameShowControllerTests.TestPlayerName
            };

            string clientId2 = Guid.NewGuid().ToString();
            Player secondPlayer = new Player("Test", null)
            {
                ClientId = clientId2,
                PlayerName = GameShowControllerTests.TestPlayerName
            };

            // Act
            using (var game = new Game(new FakeQuestionRepository(), 1, 0, 0) { GameCode = "TEST GAME" })
            {
                game.AddPlayer(player, 99);
                game.AddPlayer(secondPlayer, 99);

                // Assert
                game.NumberOfPlayers().Should().Be(2);
            }
        }

        [Fact]
        public void StartingGame_Begins_NextQuestion_Trigger()
        {
            using (var game = new Game(new FakeQuestionRepository(), 1, 0, 0) { GameCode = "TEST GAME" })
            {
                // Arrange
                bool questionAsked = false;
                ManualResetEvent methodWait = new ManualResetEvent(false);

                // Act
                game.NewQuestion += (sender, e) =>
                    {
                        questionAsked = true;
                        methodWait.Set();
                    };
                game.StartGame();

                methodWait.WaitOne(100, false);

                // Assert
                questionAsked.Should().BeTrue();
            }
        }

        [Fact]
        public void GameStops_Questions_AfterNumberHit()
        {
            using (var game = new Game(new FakeQuestionRepository(), 5, 0, 0) { GameCode = "TEST GAME" })
            {
                // Arrange
                int numberOfTimesAsked = 0;
                ManualResetEvent methodWait = new ManualResetEvent(false);

                // Act
                game.NewQuestion += (sender, e) =>
                {
                    numberOfTimesAsked = numberOfTimesAsked + 1;

                    if (numberOfTimesAsked > 4)
                    {
                        methodWait.Set();
                    }
                };
                game.StartGame();

                methodWait.WaitOne(100, false);

                // Assert
                numberOfTimesAsked.Should().Be(5);
            }
        }

        [Fact]
        public void After_AllQuestionsAsked_ThenGameOverRaised()
        {
            using (var game = new Game(new FakeQuestionRepository(), 1, 0, 0) { GameCode = "TEST GAME" })
            {
                // Arrange
                bool gameOverRaised = false;
                ManualResetEvent methodWait = new ManualResetEvent(false);

                // Act
                game.GameOver += (sender, e) =>
                {
                    gameOverRaised = true;
                    methodWait.Set();
                };
                game.StartGame();

                methodWait.WaitOne(100, false);

                // Assert
                gameOverRaised.Should().BeTrue();
            }
        }
    }
}