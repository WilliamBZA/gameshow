﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using Gameshow.Messages;
using Gameshow.DataModel;
using Gameshow;
using SignalR.Hubs;

namespace GameShow.Server.SignalR
{
    public class GameShowHub : Hub, IDisconnect
    {
        private static GameShowController _controller;
        private static Dictionary<Game, bool> _gamesWithEventsSubscribedTo = new Dictionary<Game, bool>();

        public string AddAdminConsole()
        {
            AddToGroup("Admin").Wait();

            Caller.CarryOnThen();

            return "Added!";
        }

        public AnswerQuestionResult AnswerQuestion(string answerNumber)
        {
            int answerNum = int.Parse(answerNumber);

            LogToConsole(string.Format("Player {1} answered {0} to question", answerNum, Context.ClientId));

            var game = Controller.GetPlayersCurrentGame(Context.ClientId);
            game.AnswerQuestion(Context.ClientId, answerNum);

            return new AnswerQuestionResult
            {
                EstimatedSecondsTillQuestionEnd = game.EstimatedTimeTillNextQuestion(false) + 1
            };
        }

        public void Disconnect()
        {
            this.LeaveGame();
        }

        protected void game_GameOver(object sender, GameOverEventArgs e)
        {
            Game game = sender as Game;
            var playersInGame = Clients[game.GameCode];

            playersInGame.GameOver(e.Players.OrderBy(player => player.Score).Select(player => new KeyValuePair<string, string>(player.ClientId, player.Score.ToString("N0"))).ToList());

            _gamesWithEventsSubscribedTo.Remove(game);

            game.Dispose();
        }

        protected void game_NewQuestion(object sender, QuestionEventArgs e)
        {
            LogToConsole(string.Format("New Question: {0}", e.Question.QuestionText));

            Game game = sender as Game;
            var playersInGame = Clients[game.GameCode];

            var newQuestion = new NewQuestion()
            {
                TotalQuestions = game.NumberOfQuestions,
                QuestionNumber = e.QuestionNumber,
                QuestionTitle = e.Question.QuestionText,
                PossibleAnswers = e.Question.Options.ToDictionary(i => i.Key.ToString(), i => i.Value.Text),
                QuestionExplanation = e.Question.QuestionExplanation
            };

            playersInGame.NewQuestion(newQuestion);

            System.Diagnostics.Debug.WriteLine("New Question for game " + game.GameCode);
        }

        protected void game_QuestionFinished(object sender, QuestionFinishedEventArgs e)
        {
            Game game = sender as Game;
            var playersInGame = Clients[game.GameCode];

            var allPlayers = e.CorrectPlayers.Select(kv => kv.Key).Union(e.IncorrectPlayers).Union(e.UnansweredPlayers);

            int x = 0;
            var orderedCorrectPlayers = (from playerTimespan in e.CorrectPlayers
                                         orderby playerTimespan.Value
                                         let index = x++
                                         select new
                                         {
                                             Player = playerTimespan.Key,
                                             Order = index
                                         });

            var questionFinished = new QuestionFinished
            {
                QuestionNumber = e.QuestionNumber,
                CorrectPlayers = orderedCorrectPlayers.Select(kv => new PlayerWrapper
                {
                    ClientId = kv.Player.ClientId,
                    PlayerName = kv.Player.PlayerName,
                    Order = kv.Order,
                    Score = kv.Player.Score,
                    GravatarEmail = kv.Player.GravatarEmail,
                    LastQuestionCorrect = true
                }).ToList(),
                IncorrectPlayers = e.IncorrectPlayers.Select(p => new PlayerWrapper { ClientId = p.ClientId, PlayerName = p.PlayerName, Score = p.Score, Order = 999, GravatarEmail = p.GravatarEmail, LastQuestionCorrect = false }).ToList(),
                UnansweredPlayers = e.UnansweredPlayers.Select(p => new PlayerWrapper { ClientId = p.ClientId, PlayerName = p.PlayerName, Score = p.Score, Order = 999, GravatarEmail = p.GravatarEmail, LastQuestionCorrect = false }).ToList(),
                EstimatedTimeTillNextQuestion = game.EstimatedTimeTillNextQuestion(false)
            };

            playersInGame.QuestionCompleted(questionFinished);
        }

        public PlayerJoinedResult JoinGame(string playerName, string gravatarEmail)
        {
            var player = Controller.AddPlayer(Context.ClientId, playerName, gravatarEmail);
            var game = Controller.JoinGame(player.ClientId);

            if (!_gamesWithEventsSubscribedTo.ContainsKey(game))
            {
                game.NewQuestion += new Game.NewQuestionHandler(game_NewQuestion);
                game.QuestionFinished += new Game.QuestionFinishedHandler(game_QuestionFinished);
                game.GameOver += new Game.GameOverHandler(game_GameOver);

                _gamesWithEventsSubscribedTo.Add(game, true);
            }

            AddToGroup(game.GameCode).ContinueWith(t =>
            {
                System.Diagnostics.Debug.WriteLine("ClientID: " + Context.ClientId + " join Game: " + game.GameCode);
            });

            Caller.CarryOnThen(); // Let them know they've joined correctly

            LogToConsole(string.Format("Player joined\nClient ID: {0}         Player Name: {1}\nTotal Players : {2}\nTotal Games : {3}\n", player.ClientId, player.PlayerName, Controller.NumberOfPlayers(), Controller.NumberOfGames()));

            return new PlayerJoinedResult
            {
                ClientId = player.ClientId,
                EstimatedSecondsTillNextQuestion = game.EstimatedTimeTillNextQuestion(true)
            };
        }

        public void LeaveGame()
        {
            var game = Controller.GetPlayersCurrentGame(Context.ClientId);
            if (game != null)
            {
                //RemoveFromGroup(game.GameCode);
            }

            Controller.RemovePlayer(Context.ClientId);

            LogToConsole(string.Format("Player left\nTotal Players : {0}\nTotal Games : {1}\n", Controller.NumberOfPlayers(), Controller.NumberOfGames()));

            System.Diagnostics.Debug.WriteLine("Removing ClientID: " + Context.ClientId);
        }

        private void LogToConsole(string message)
        {
            var adminClients = Clients["Admin"];

            adminClients.MessageReceived(message);
        }

        public GameShowController Controller
        {
            get
            {
                _controller = _controller ?? new GameShowController();
                return _controller;
            }
        }
    }
}

